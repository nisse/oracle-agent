package main

import (
	"crypto/ed25519"
	"fmt"
	"log"
	"net"
	"os"
	"os/exec"
	"syscall"

	"git.glasklar.is/nisse/oracle-agent"
)

// Generate an Ed25519 key pair, add it to an agent, and spawn process
// in agent environment.
func main() {
	if len(os.Args) < 3 {
		log.Fatalf("Usage: $0 SOCKET-NAME CMD ...", os.Args[0])
	}
	socketName := os.Args[1]

	_, signer, err := ed25519.GenerateKey(nil)
	if err != nil {
		log.Fatalf("Keygen failed: %v", err)
	}
	agentKey, err := agent.AgentKeyFromEd25519(signer)
	if err != nil {
		log.Fatalf("Internal error: %v", err)
	}
	oldMask := syscall.Umask(0077)
	l, err := net.Listen("unix", socketName)
	if err != nil {
		log.Fatalf("Failed to listen on UNIX socket:", err)
	}
	// defer seems unreliable, since we call os.Exit
	defer os.Remove(socketName)
	syscall.Umask(oldMask)

	cmd := exec.Command(os.Args[2], os.Args[3:]...)
	cmd.Env = append(cmd.Environ(), fmt.Sprintf("SSH_AUTH_SOCK=%s", socketName))
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Start(); err != nil {
		log.Fatalf("Failed to start process: %v", err)
	}

	go func() {
		// Exit after the command has completed, propagate exit code.
		if err := cmd.Wait(); err != nil {
			log.Fatalf("Wait failed: %v", err)
		}
		os.Remove(socketName) // Ignore error
		if cmd.ProcessState.Exited() {
			os.Exit(cmd.ProcessState.ExitCode())
		}
		log.Fatal("Unexpected process exit: %v", cmd.ProcessState)
	}()

	for {
		c, err := l.Accept()
		if err != nil {
			log.Fatalf("Accept failed: %#v\n", err)
		}
		go agent.ServeAgent(c, c, agentKey)
	}
}
